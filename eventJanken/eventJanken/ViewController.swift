//
//  ViewController.swift
//  eventJanken
//
//  Created by 藤原 章敬 on 2018/10/17.
//

import UIKit
import AVKit
import AVFoundation

class ViewController: UIViewController {
    
    @IBOutlet weak var videoTitle: UILabel!
    @IBOutlet weak var playerview: AVPlayerView!
    
    @IBOutlet weak var startImage: UIImageView!
    
    var urlString:String = ""
    var isDsiPlayCover = true
    
    var controller:AVPlayerViewController?
    var asset:AVURLAsset? = nil
    var playerItem:AVPlayerItem? = nil
    var player:AVPlayer? = nil
    
    
    let prizeList = [
        Prize(name: "midori_goo_fix", probability:11.1111111 ),
        Prize(name: "midori_choki_fix", probability:11.1111111 ),
        Prize(name: "midori_par_fix", probability:11.1111111),
        Prize(name: "himawari_goo_fix", probability:11.1111111),
        Prize(name: "himawari_choki_fix", probability:11.1111111),
        Prize(name: "himawari_par_fix", probability:11.1111111),
        Prize(name: "laki_goo_fix", probability:11.1111111),
        Prize(name: "laki_choki_fix", probability:11.1111111),
        Prize(name: "laki_par_fix", probability:11.1111111)
    ]
    
    let jyunban = [
        "midori_goo_fix",
        "midori_choki_fix",
        "midori_par_fix",
        
        "himawari_goo_fix",
        "himawari_choki_fix",
        "himawari_par_fix",
        
        "laki_goo_fix",
        "laki_choki_fix",
        "laki_par_fix",
    ]
    
    var now_count = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
      //  self.videoTitle.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/2)*3)
        
        
        
        // タップされた時のアクションを登録
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.pushName(_:)))
        playerview.addGestureRecognizer(tap)
        
        let startTap = UITapGestureRecognizer(target: self, action: #selector(self.pushStart(_:)))
        startImage.addGestureRecognizer(startTap)
        
        // myLabel自体のタップを検知するようにする
        playerview.isUserInteractionEnabled = true
        startImage.isUserInteractionEnabled = true
        
        // バックグラウンドでも再生を続けるための設定
        let audioSession = AVAudioSession.sharedInstance()
        
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayback)
            try audioSession.setActive(true)
        } catch let error as NSError {
            print(error)
        }
        
        controller = AVPlayerViewController()
        

        
//        let yurl = URL(string: urlString)
//        asset = AVURLAsset(url: yurl! , options: nil)
        

        
        //動画が終わったタイミングを監視
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        
        
        // フォアグラウンド復帰時の通知を設定する
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(ViewController.onWillForGround(_:)),
            name: NSNotification.Name.UIApplicationWillEnterForeground,
            object: nil
        )
        //
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(ViewController.onEnterBackGround(_:)),
            name: NSNotification.Name.UIApplicationDidEnterBackground,
            object: nil
        )
        
        

    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // フォアグラウンド復帰時に行う処理
    @objc func onWillForGround(_ notification: Notification?) {
        //
        playerview.player().play()
    }
    //
    @objc func onEnterBackGround(_ notification: Notification?) {
        //
        playerview.player().pause()
    }
    
    //動画を一番最初まで巻き戻す
    @objc func playerItemDidReachEnd(_ notification: Notification) {
        
       // playerview.player().seek(to: kCMTimeZero)
        playerview.player().pause()
        self.isDsiPlayCover = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.playerview.player().pause()
//        self.playerview.player().seek(to: kCMTimeZero)
    }
    // 監視対象の値に変化があった時に呼ばれる
//    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {}
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func cyusen () {
        let prizesNo = arc4random_uniform(10000)
        ///1万を最大値にとる乱数
        
        let prizes = prizeList.count-1
        var k = 0
        
        print(prizes)
        
        for prize in prizeList{
            k = k + prize.number()
            if Int(prizesNo) < k {
                print(prize.name)   //ここで抽選結果を表示する
                self.urlString = prize.name
                
            var chName = ""
            if (self.urlString.contains("laki_")) {
                    chName = "虹河ラキ"
            } else if (self.urlString.contains("hima_")) {
                chName = "ひまわり"
            } else if (self.urlString.contains("midori_")) {
                chName = "みどり"
            }
                print(chName)
           //     self.videoTitle.text = chName
                break
            }
     //       print(k)
        }
    
        //jyunban
//        urlString = jyunban[now_count]
//        now_count = now_count + 1
//        
//        if (now_count >= jyunban.count) {
//            now_count = 0
//            urlString = jyunban[8]
//        }
//        print("NOW::\(urlString)")
        
        
        // 動画を読み込み、動画プレイヤーに設定
        if let path = Bundle.main.path(forResource: self.urlString, ofType: "mp4") {
            let url = URL(fileURLWithPath: path)
            asset = AVURLAsset(url: url, options: nil)
        }
        asset?.resourceLoader.setDelegate(self as? AVAssetResourceLoaderDelegate, queue: DispatchQueue.main)
        playerItem = AVPlayerItem(asset: asset!)
        player = AVPlayer(playerItem: playerItem)
        playerview.setPlayer(player: player!)
        
        player?.actionAtItemEnd = AVPlayerActionAtItemEnd.none
        let playerLayer: AVPlayerLayer = AVPlayerLayer(player: playerview.player())
        playerLayer.frame = UIScreen.main.bounds
        playerview.backgroundColor = UIColor.clear
        playerview.setVideoFillMode(fillMode: AVLayerVideoGravity.resizeAspect.rawValue)
    }
    
    @IBAction func pushCyusen(_ sender: UIButton) {

        
    }
    
    @objc func pushStart(_ sender: UITapGestureRecognizer) {
        self.isDsiPlayCover = false
        self.startImage.fadeOut(type: .Slow)
        //testLog()
        self.cyusen()
        playerview.player().play()
    
    }
    
    @objc func pushName(_ sender: UITapGestureRecognizer) {
        
        if (isDsiPlayCover) {
         self.startImage.fadeIn(type: .Normal)
        } 
        
        
        
//        print("tap::\(isPause)")
//        if (self.isPause) {
//            playerview.player().play()
//            self.isPause = false
//        } else {
//            playerview.player().pause()
//            self.isPause = true
//        }
        
    }
    
    //-------Unit-Test
    
    func testLog() {
        let countMax = 1000
        var laki_time = 0
        var laki_time2 = 0
        var laki_time3 = 0
        
        var middori_time = 0
        var middori_time2 = 0
        var middori_time3 = 0
        
        var himawari_time = 0
        var himawari_time2 = 0
        var himawari_time3 = 0
        
        
    
        for counter in 1...countMax {
            
            cyusenTest()
            print(counter)
            var chName = ""
            if (self.urlString.contains("raki_")) {
                chName = "虹河ラキ"
                if (self.urlString.contains("raki_goo")) {
                    laki_time = laki_time + 1
                } else if (self.urlString.contains("raki_choki")) {
                    laki_time2 = laki_time2 + 1
                } else if (self.urlString.contains("raki_par")) {
                    laki_time3 = laki_time3 + 1
                }
                print("\(chName)\(laki_time+laki_time2+laki_time3)回")
                
            } else if (self.urlString.contains("himawari_")) {
                chName = "ひまわり"
                if (self.urlString.contains("himawari_goo")) {
                    himawari_time = himawari_time + 1
                } else if (self.urlString.contains("himawari_choki")) {
                    himawari_time2 = himawari_time2 + 1
                } else if (self.urlString.contains("himawari_par")) {
                    himawari_time3 = himawari_time3 + 1
                }
                print("\(chName)\(himawari_time+himawari_time2+himawari_time3)回")
                
            } else if (self.urlString.contains("midori_")) {
                chName = "みどり"
                if (self.urlString.contains("midori_goo")) {
                    middori_time = middori_time + 1
                } else if (self.urlString.contains("midori_choki")) {
                    middori_time2 = middori_time2 + 1
                } else if (self.urlString.contains("midori_par")) {
                    middori_time3 = middori_time3 + 1
                }
                
                print("\(chName)\(middori_time+middori_time2+middori_time3)回")
            }
            
            
            
        }
        print("抽選回数：\(countMax)回")
        print("ラキ_グー：\(laki_time)回")
        print("ラキ_チョキ：\(laki_time2)回")
        print("ラキ_パー：\(laki_time3)回")
        
        print("みどり_グー：\(middori_time)回")
        print("みどり_チョキ：\(middori_time2)回")
        print("みどり_パー：\(middori_time3)回")
        
        
        print("ひまわり_グー：\(himawari_time)回")
        print("ひまわり_チョキ：\(himawari_time2)回")
        print("ひまわり_パー：\(himawari_time3)回")
    }
    
    func cyusenTest() {
        let prizesNo = arc4random_uniform(10000)
   //     let prizes = prizeList.count-1
        var k = 0
        
       // print(prizes)
        
                for prize in prizeList{
                    k = k + prize.number()
                    if Int(prizesNo) < k {
                        print(prize.name)   //ここで抽選結果を表示する
                        self.urlString = prize.name
                        
                        break
                    }
                    
                }
        
    }
    

}





//--------抽選用クラス---------------------------------------------

class Prize:NSObject {
    var name: String
    var probability: Double
    var prizesNo = 0
    let lotNo = 10000.0
    
    
    init(name: String, probability: Double){
        self.name = name
        self.probability = probability
    }
    
    func number() -> Int{
        var result: Int
        result = Int(Double(lotNo) * Double(probability/100))
        return result
    }
}
//----------------------------------------------------------------
