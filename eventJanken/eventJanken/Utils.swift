//
//  Utils.swift
//  objcTEST
//
//  Created by 藤原　章敬　 on 2018/06/19.
//  Copyright © 2018年 藤原　章敬　. All rights reserved.
//

let jsonString = """
{
{
"title" :"Youtube動画",
"movie_path" :"xdefi4n3y",
"thamb_image" :"xdefi4n3y".
"playtime" :"00:02:30",
"uodate" :"2011/3/11",
"visible_type" :"0",

},
[
title:"twitter動画",
movie_path:"https://tw/movie/mov.mp4",
thamb_image:"https://tw/img/img.jpg".
playtime:"00:02:30",
uodate:"2011/3/11",
visible_type:"0",

]

}


"""


/*
 
おじチェン
 ＜おじのこえ＞
 
chin-none
 pole-ball
 ari-ari
 ari-nasi
 nasi-nasi
 
 
 
 ・通信チェック  レ
 ・レビュー機能    レ
 ・トースト表示    レ
　・オンライン画像ローダー    レ
 
 ・ダイアログ表示（画像付き・選択肢）x
 ・バナー広告     x
 ・全画面広告     x
 ・シェア機能　x
 ・通信によるJson取得
 ・通信によるJson保存
 
 vc
・コンテンツ一覧テーブル
・Youtube動画再生ビュー
・MP4動画再生ビュー
 
 コレクションビュー
 情報入力ビュー
 
 ダウンスワイパブルVC
 
 UIView上にSpriteKitエフェクト

 ・カメラ入力・キャプチャ＆ホワイトバランス調整
 
 
・タッピックエンジン＆バイブ
・どっトースト

 ・UIImageViewパラパラアニメ
 ・画面サイズ情報
 ・GA送信
 ・FirebaseA送信
 
 ・ReplayKit
 
 ・URLからMP3再生
 ・音声録音
 ・アップロード
・設定画面
・インフォメーション画面（UIwebview）
 
 ・ナビゲーション
 
*/


import Foundation
import UIKit
import StoreKit
import AVFoundation
import AVKit
import SystemConfiguration

//----------------------------------------------------
//  パディング設定可能ラベル。
//----------------------------------------------------
class PaddingLabel: UILabel {
    
    @IBInspectable var padding: UIEdgeInsets = UIEdgeInsets(top: 4, left: 8, bottom: 4, right: 8)
    
    override func drawText(in rect: CGRect) {
        let newRect = UIEdgeInsetsInsetRect(rect, padding)
        super.drawText(in: newRect)
    }
    
    override var intrinsicContentSize: CGSize {
        var contentSize = super.intrinsicContentSize
        contentSize.height += padding.top + padding.bottom
        contentSize.width += padding.left + padding.right
        return contentSize
    }
    
}
//----------------------------------------------------
//レビュー表示処理
//----------------------------------------------------
class DisplayReview: NSObject {
    
    func dispReview() {
        // レビューページへ遷移
        if #available(iOS 10.3, *) {
            
            SKStoreReviewController.requestReview()
        }
            // iOS 10.3未満の処理
        else {
            if let url = URL(string: "itms-apps://itunes.apple.com/app/id1219171108?action=write-review") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:])
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
}
//--------------------------------------------------
//AVPlayerView
//--------------------------------------------------
final class AVPlayerView : UIView {
    override public class var layerClass: Swift.AnyClass {
        get {
            return AVPlayerLayer.self
        }
    }
    
    private var playerLayer: AVPlayerLayer {
        return self.layer as! AVPlayerLayer
    }
    
    func player() -> AVPlayer {
        return playerLayer.player!
    }
    
    func setPlayer(player: AVPlayer) {
        playerLayer.player = player
    }
    
    func setVideoFillMode(fillMode: String) {
        playerLayer.videoGravity = AVLayerVideoGravity(rawValue: fillMode)
    }
    
    func videoFillMode() -> String {
        return playerLayer.videoGravity.rawValue
    }
    
    func setBackGroundPlay() {
        // バックグラウンドでも再生を続けるための設定
        let audioSession = AVAudioSession.sharedInstance()
        
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayback)
            try audioSession.setActive(true)
        } catch let error as NSError {
            print(error)
        }
        
    }

}
//----------------------------------------
// uiview fadein fadeout
//----------------------------------------
enum FadeType: TimeInterval {
    case
    Normal = 0.5,
    Slow = 0.8
}

extension UIView {
    func fadeIn(type: FadeType = .Normal, completed: (() -> ())? = nil) {
        fadeIn(duration: type.rawValue, completed: completed)
    }
    
    /** For typical purpose, use "public func fadeIn(type: FadeType = .Normal, completed: (() -> ())? = nil)" instead of this */
    func fadeIn(duration: TimeInterval = FadeType.Slow.rawValue, completed: (() -> ())? = nil) {
        alpha = 0
        isHidden = false
        UIView.animate(withDuration: duration,
                       animations: {
                        self.alpha = 1
        }) { finished in
            completed?()
        }
    }
    func fadeOut(type: FadeType = .Normal, completed: (() -> ())? = nil) {
        fadeOut(duration: type.rawValue, completed: completed)
    }
    /** For typical purpose, use "public func fadeOut(type: FadeType = .Normal, completed: (() -> ())? = nil)" instead of this */
    func fadeOut(duration: TimeInterval = FadeType.Slow.rawValue, completed: (() -> ())? = nil) {
        UIView.animate(withDuration: duration
            , animations: {
                self.alpha = 0
        }) { [weak self] finished in
            self?.isHidden = true
            self?.alpha = 1
            completed?()
        }
    }
}

//--------------------------------------------------
//画像ローダー
//--------------------------------------------------
class AsyncImageView: UIImageView {
    let CACHE_SEC : TimeInterval = 5 * 60; //5分キャッシュ
    
    //画像を非同期で読み込む
    func loadImage(urlString: String){
        
        let indi = ViewIndicator()
        indi.showIndicator(loadingView: self)
        
        let req = URLRequest(url: NSURL(string:urlString)! as URL,
                             cachePolicy: .returnCacheDataElseLoad,
                             timeoutInterval: CACHE_SEC);
        let conf =  URLSessionConfiguration.default;
        let session = URLSession(configuration: conf, delegate: nil, delegateQueue: OperationQueue.main);
        
        session.dataTask(with: req, completionHandler:
            { (data, resp, err) in
                
                if((err) == nil){ //Success
                    let image = UIImage(data:data!)
                    self.image = image;
                    
                }else{ //Error
                    print("AsyncImageView:Error ")  //\(err?.localizedDescription ?? "")
                }
        }).resume();
    }
}
//---------------------------------------------
// httpの音楽ファイルを非同期で取得して再生する
//---------------------------------------------
class NetMusicPlay : NSObject {
    
    func mplay(murl:String,pview:UIView) {
        var player = AVAudioPlayer()
        
        DispatchQueue.global(qos: .default).async {
            
            let url = URL(string: "http://xxxxx.m4a")
            let data = NSData(contentsOf: url!)
            
            let indi = ViewIndicator()
            indi.showIndicator(loadingView: pview)
            
                // Main
                DispatchQueue.main.async {
                    do {
                        indi.hideIndicator()
                        player = try AVAudioPlayer(data: data! as Data)
                        player.prepareToPlay()
                        player.play()
                    } catch {
                        indi.hideIndicator()
                        NSLog("cannot play audio")
                    }
                }
            
        }
    }

}
//--------------------------------------------------
//  indicator
//--------------------------------------------------
class ViewIndicator:NSObject {
     let indicator = UIActivityIndicatorView()
    
    func showIndicator(loadingView:UIView) {
        
        // UIActivityIndicatorView のスタイルをテンプレートから選択
        indicator.activityIndicatorViewStyle = .whiteLarge
        // 表示位置
        indicator.center = loadingView.center
        // 色の設定
        indicator.color = UIColor.white
        // アニメーション停止と同時に隠す設定
        indicator.hidesWhenStopped = true
        // loadingViewに追加
        loadingView.addSubview(indicator)
        // 最前面に移動
        loadingView.bringSubview(toFront: indicator)
        // アニメーション開始
        indicator.startAnimating()
        
        // 3秒後にアニメーションを停止させる
        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
            self.indicator.stopAnimating()
        })
    }
    
    func hideIndicator() {
        indicator.stopAnimating()
    }
}

//--------------------------------------------------
// swift reachability
//--------------------------------------------------
/* 使い方 swift
 print(ReachabilityChecker.check("google.com"))
 true->OK
 false->NG
 */
class ReachabilityChecker : NSObject {
    func check(host_name:String)->Bool{
        
        let reachability = SCNetworkReachabilityCreateWithName(nil, host_name)!
        var flags = SCNetworkReachabilityFlags.connectionAutomatic
        if !SCNetworkReachabilityGetFlags(reachability, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}
//--------------------------------------------------
//Aramofire通信
//--------------------------------------------------
//class HttpCliant : NSObject {
//    
//    var username = ""
//    var password = ""
//    var urlString = ""
//    
//    func createRequest(url:String, parameters: Parameters? = nil) -> Alamofire.DataRequest {
//        let manager = Alamofire.SessionManager.default
//        // タイムアウト
//        manager.session.configuration.timeoutIntervalForRequest = 300
//        
//        return manager.request(url,
//                               // メソッド
//            method:.get,
//            parameters: parameters,
//            encoding: JSONEncoding.default,
//            // 共通ヘッダー
//            headers: ["Accept": "application/json"]).validate()
//    }
//    
//    func HttpPostRequest() {
//        let url = urlString
//        let headers: HTTPHeaders = [
//            "Contenttype": "application/json"
//        ]
//        let parameters:[String: Any] = [
//            "username": username,
//            "password": password
//        ]
//        
//        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
//            if let result = response.result.value as? [String: Any] {
//                print(result)
//            }
//        }
//        
//    }
//}

//--------------------------------------------------
//
//--------------------------------------------------
//
//--------------------------------------------------
//
//--------------------------------------------------

/*
 [2017年版]RxSwift + Alamofire + ObjectMapper + RealmのSwift実装について
 https://qiita.com/syou007/items/8f27e7f0d03b9cfcff6c
 
 モデルの作成
 import ObjectMapper
 
 class Prefecture: Mappable{
 // 都道府県名
 dynamic var name = ""
 
 required init?(map: Map) {}
 
 // Mappable
 func mapping(map: Map) {
 name <- map["name"]
 }
 }
 
 
 
 
 
 JSON取得ーモデル変換
 
 import Alamofire
 import ObjectMapper
 import AlamofireObjectMapper
 
 ...
 
 Alamofire.request("http://localhost/api/pref.json").responseArray() { (response: DataResponse<[Prefecture]>) in
 switch response.result {
 case .success(let prefectures):
 // データ取得成功（[Prefectures]としてデータが取得できてます。）
 print("Prefectures: \(prefectures)")
 case .failure(let error):
 // データ取得エラー
 }
 }
 
 
 次は取得してきたデータをRealmに保存します。
 Realmに保存するためにはモデルを以下のように少し変える必要があります。
 // [変更]RealmSwiftを追加する。
 import RealmSwift
 import ObjectMapper
 
 // [変更]Objectクラスを継承する。
 class Prefecture: Object, Mappable {
 // 都道府県名
 dynamic var name = ""
 
 // [変更]この部分を以下のように変更する。
 required convenience init?(map: Map) {
 self.init()
 }
 
 // Mappable
 func mapping(map: Map) {
 name <- map["name"]
 }
 }
 モデルを変更した後にAPI取得後の部分を変更します。
 Alamofire.request("http://localhost/api/pref.json").responseArray() { (response: DataResponse<[Prefecture]>) in
 switch response.result {
 case .success(let prefectures):
 // データ取得成功したの保存します。
 let realm = try! Realm()
 try! realm.write {
 // 「update: true」のオプションに関しては今回は割愛します。
 realm.add(prefectures)
 }
 case .failure(let error):
 // データ取得エラー
 }
 }
 
 
 ＊＊＊
 
 ここまででデータの取得と保存ができていますが、このままでは取得したデータを使うのに手間がかかります。
 例えば、コールバックを使うとした場合は以下のようになります。
 
 func request(callback:([Prefecture])->()) {
 Alamofire.request("http://localhost/api/pref.json").responseArray() { (response:
 DataResponse<[Prefecture]>) in
 switch response.result {
 case .success(let prefectures):
 // データ取得成功したの保存します。
 let realm = try! Realm()
 try! realm.write {
 // 「update: true」のオプションに関しては今回は割愛します。
 realm.add(prefectures)
 }
 callback(prefectures)
 case .failure(let error):
 // データ取得エラー
 }
 }
 }
 上記のようなコードを書いて
 request() { (prefectures) in
 print(prefectures)
 }
 簡単なアプリケーションを作るならコールバックを使って構築してもそんなに問題になることはありません。
 しかし、APIを同時に２本走らせて両方の結果を待つ場合など複雑なことをやろうとした場合にコールバック処理で構築しているとコールバック地獄に陥る可能性があります。
 RxSwiftはそんな時に力を発揮するテクノロジーです。
 RxSwiftの導入
 
 今回は踏み込んで書きませんが、RxSwiftはObserverパターンを使って処理を伝播させます。
 RxSwiftのコードが完成すれば、GitHubに書かれているサンプルの処理がほぼ全て行える事になります。
 僕の経験上では、複数APIの待ち受けやAPIの結果を元に次のAPIを実行する場合などに重宝しました。
 先程のコールバックパターンをRxSwiftに置き換えます。
 // [変更] 戻り値を追加
 func request() -> Observable<[Prefecture]> {
 // [変更] Observableを返却
 return Observable.create { (observer: AnyObserver<[Prefecture]>) in
 Alamofire.request("http://localhost/api/pref.json").responseArray() { (response:
 DataResponse<[Prefecture]>) in
 switch response.result {
 case .success(let prefectures):
 // データ取得成功したの保存します。
 let realm = try! Realm()
 try! realm.write {
 // 「update: true」のオプションに関しては今回は割愛します。
 realm.add(prefectures)
 }
 
 // [変更] 通知の処理は「observer」に任せる。
 observer.on(.next(prefectures))
 observer.onCompleted()
 case .failure(let error):
 // データ取得エラー
 
 // [変更] 通知の処理は「observer」に任せる。
 observer.onError(error)
 }
 }
 return Disposables.create()
 }
 }
 ObservableはObserverが通知した情報を受取る仕組みです。
 Observableを受け取るため、使用する側の処理は以下のようになります。
 request().subscribe(onNext: { (prefectures) in
 print(prefectures)
 }, onError: {...})
 subscribeを行うことで、Observableから通知を受け取れるようにします。
 RxSwiftではデータの伝播をObservableを通じて行うので、インターフェースの統一ができます。
 これでRxSwiftの対応はおしまいです。
 ちょっと古い記事ですが、RxSwiftのメモリ管理についてはこちらに記事を書いているのでよければみてください。
 おまけ　その１
 
 マスターデータを取得する場合に、キャッシュを使う場合もあると思います。
 以下コードの「キャッシュがある場合」の箇所にキャッシュ保持時間の条件を入れれば「キャッシュがあればキャッシュを返し、なければAPIからデータを取得して返す」という処理になります。
 func request() -> Observable<[Prefecture]> {
 return Observable.create { (observer: AnyObserver<[Prefecture]>) in
 // [追加] キャッシュがある場合
 if キャッシュがある場合 {
 observable.on(.next(Prefecture.all()))
 observable.onCompleted()
 return Disposables.create()
 }
 
 Alamofire.request("http://localhost/api/pref.json").responseArray() { (response:
 DataResponse<[Prefecture]>) in
 switch response.result {
 case .success(let prefectures):
 // データ取得成功したの保存します。
 let realm = try! Realm()
 try! realm.write {
 // [変更]エリアデータは自動的に消えないため、手動で削除する。
 realm.delete(realm.objects(Area.self))
 realm.add(prefectures, update: true)
 }
 
 observer.on(.next(prefectures))
 observer.onCompleted()
 case .failure(let error):
 // データ取得エラー
 observer.onError(error)
 }
 }
 return Disposables.create()
 }
 }
 呼び出し元は特に変更する必要はありません。
 おまけ　その２
 
 APIの処理と描画処理はスレッドが別になります。
 なので、APIのデータが戻ってきた後に処理をメインスレッドに渡す必要があります。
 RxSwiftを使う場合は以下の記載でスレッドを合わせることができます。
 request().observeOn(MainScheduler.instance).subscribe(onNext: { (prefectures) in
 // UIを変更する
 }, onError: {...})
 observeOn(MainScheduler.instance)を追加するだけでメインスレッドとして処理できるようになります。

 
*/
